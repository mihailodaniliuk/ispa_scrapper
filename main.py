import csv
import os
import urllib.request

import requests
from bs4 import BeautifulSoup
import pandas as pd
import wget
import urllib.error
import re
import shutil

regex = '.(\w+)$'


def get_200_response(url, params=None):
    while True:
        try:
            response = requests.get(url, params=params)
            if response.status_code == 200:
                return response
        except Exception as e:
            print(e)


def get_all_company_urls():
    site_company_links = []
    response = requests.get("https://ispa.org.za/membership/list-of-members/")
    soup = BeautifulSoup(response.text, "html.parser")
    all_trs = soup.find("div", class_="grid_9 buffer content memberlist").find_all("tr")
    for tr in all_trs:
        if tr.find("th"):
            pass
        else:
            site_company_links.append(tr.find_all("td")[2].find("a").get("href"))
    return site_company_links


def create_directory():
    directory = 'logo_folder'
    try:
        path = os.path.join(os.getcwd(), directory)
        os.mkdir(path)
    except Exception as e:
        print(e)


def delete_directory(directory):
    try:
        shutil.rmtree(directory)
    except Exception as e:
        print(e)


def download_logo(link, name):
    print(link)
    if link != 'https://ispa.org.za/logo/':
        extension = re.findall(regex, link)[0]
        try:
            wget.download(link, f'./logo_folder/{name}.{extension}')
        except urllib.error.HTTPError:
            print('No logo')


def get_company_info(link):
    company_name, description, website, email, phone, licenses = '', '', '', '', '', ''
    company_contact_url, registered_name, address, services, logo_url = '', '', '', '', ''
    resp = get_200_response(link)
    soup = BeautifulSoup(resp.text, "html.parser")
    # print(soup)
    table_blocks = soup.find('div', class_='bodytext').find_all('table')
    about_table, details_table, links_table, compliance_table, services_table = None, None, None, None, None
    for table_block in table_blocks:
        for tr in table_block.find_all('tr'):
            if tr.find("td"):
                if tr.find("td").find("div", class_="section"):
                    about_table = table_block
                    break
                elif tr.find("td").text == 'Member details':
                    details_table = table_block
                    break
                elif tr.find("td").text == 'Compliance links':
                    links_table = table_block
                    break
                elif tr.find("td").text == 'Compliance information':
                    compliance_table = table_block
                    break
                elif tr.find("td").text == 'Services offered':
                    services_table = table_block
                    break
    for about_tr in about_table.find_all('tr'):
        if about_tr.find("h2"):
            company_name = about_tr.find("h2").text.strip()
        elif (description == '') and (about_tr.find("td").find("div", class_="section")):
            if 'DetailsLinksComplianceStatementsServices' not in about_tr.find("td").find("div",
                                                                                          class_="section").text.strip():
                description = about_tr.find("td").find("div", class_="section").text

    logo_url = f"https://ispa.org.za{about_table.find('img', class_='img-member').get('src')}"

    for details_tr in details_table.find_all('tr')[1:]:
        if "Website:" in details_tr.find_all("td")[0].text:
            website = details_tr.find_all("td")[2].text.strip()
        elif "Support email:" in details_tr.find_all("td")[0].text:
            email = details_tr.find_all("td")[2].text
        elif "Support phone:" in details_tr.find_all("td")[0].text:
            phone = details_tr.find_all("td")[2].text
        elif "Licences:" in details_tr.find_all("td")[0].text:
            licenses = details_tr.find_all("td")[2].text
        elif details_tr.find_all("td")[0].text == '':
            licenses += f'\n{details_tr.find_all("td")[2].text}'

    for links_tr in links_table.find_all('tr'):
        tds = links_tr.find_all("td")
        for td in tds:
            if td.text == 'Company contacts':
                company_contact_url = td.find('a').get('href').strip('/')
    for compliance_tr in compliance_table.find_all('tr'):
        if "Registered name:" in compliance_tr.find_all("td")[0].text:
            registered_name = compliance_tr.find_all("td")[2].text.strip()
        elif "Physical address:" in compliance_tr.find_all("td")[0].text:
            address = compliance_tr.find_all("td")[2].text
        elif compliance_tr.find_all("td")[0].text == '':
            address += f', {compliance_tr.find_all("td")[2].text}'
    services = services_table.text.replace('Services offered', '').strip()
    return (company_name, description, website, email, phone, licenses,
            company_contact_url, registered_name, address, services, logo_url)


if __name__ == "__main__":
    delete_directory(f'{os.getcwd()}/logo_folder')
    create_directory()
    links = get_all_company_urls()
    with open('result.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(
            ('Name', 'Description', 'Website', 'Support email', 'Support phone', 'Licenses',
             'Company contacts URL', 'Registered name', 'Physical address', 'Services'))
        for site_company_link in links:
            company_info = get_company_info(site_company_link)
            print(company_info[0])
            writer.writerow(company_info[:-1])
            if company_info[-1]:
                download_logo(company_info[-1], company_info[0])
